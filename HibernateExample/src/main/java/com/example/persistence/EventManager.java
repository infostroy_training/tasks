package com.example.persistence;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import com.example.entity.Event;
import com.example.entity.Person;

public class EventManager {

	public void createAndStoreEvent(String title, Date date) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Event event = new Event();
		event.setTitle(title);
		event.setDate(date);
	
		session.save(event);
		
		session.getTransaction().commit();
	}
	
	public void storePerson(Person person) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	
		session.save(person);
		
		session.getTransaction().commit();
	}

	public List<Event> listEvents() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		List<Event> result = session.createCriteria(Event.class).list();

		session.getTransaction().commit();
		return result;
	}

	public void addPersonToEvent(Long personId, Long eventId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Person person = (Person) session.load(Person.class, personId);
		Event event = (Event) session.load(Event.class, eventId);
		person.getEvents().add(event);

		session.getTransaction().commit();
	}

	public void addEmailToPerson(Long personId, String emailAddress) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Person aPerson = (Person) session.load(Person.class, personId);
		
		// adding to the emailAddress collection might trigger a lazy load of
		// the collection
		
		aPerson.getEmailAddresses().add(emailAddress);

		session.getTransaction().commit();
	}
}