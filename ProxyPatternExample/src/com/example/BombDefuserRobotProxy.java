package com.example;

public class BombDefuserRobotProxy extends BombDefuserRobot {

	private BombDefuserRobot bombDefuserRobot;
	private int connectionWaveLenght;
	private int connectionAttempts = 3;

	public BombDefuserRobotProxy(int connectionWaveLength) {
		this.connectionWaveLenght = connectionWaveLength;
		bombDefuserRobot = new BombDefuserRobot();
	}

	@Override
	public void moveForward(int steps) {
		ensureConnectionWithRobot();
		bombDefuserRobot.moveForward(steps);
	}

	@Override
	public void turnLeft() {
		ensureConnectionWithRobot();
		bombDefuserRobot.turnLeft();
	}

	@Override
	public void turnRight() {
		ensureConnectionWithRobot();
		bombDefuserRobot.turnRight();
	}

	@Override
	public void defuseBomb() {
		ensureConnectionWithRobot();
		bombDefuserRobot.defuseBomb();
	}

	private void ensureConnectionWithRobot() {
		if (bombDefuserRobot == null) {
			bombDefuserRobot = new BombDefuserRobot();
			bombDefuserRobot.connectWireless(connectionWaveLenght);
		}
		for (int i = 0; i < connectionAttempts; i++) {
			if (!bombDefuserRobot.isConnected()) {
				bombDefuserRobot.connectWireless(connectionWaveLenght);
			} else {
				break;
			}
		}
		if (!bombDefuserRobot.isConnected()) {
			throw new BadConnectionException("No connection with remote bomb diffuser robot could be made after few attempts.");
		}
	}
}
