package com.example;

public interface Robot {

	void moveForward(int steps);
	void turnLeft();
	void turnRight();
	void defuseBomb();
}
