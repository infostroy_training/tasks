package com.example;

import java.lang.reflect.Proxy;

public class Demo {
	public static void main(String[] args) {
		BombDefuserRobot robot = new BombDefuserRobot();
		Robot robotProxy = (Robot) Proxy.newProxyInstance(BombDefuserRobot.class.getClassLoader(),
														  BombDefuserRobot.class.getInterfaces(),
														  new RobotInvocationHandler(robot,35));
		
		robotProxy.moveForward(10);
		robotProxy.turnRight();
		robotProxy.moveForward(3);
		robotProxy.defuseBomb();		
		
	}
}
