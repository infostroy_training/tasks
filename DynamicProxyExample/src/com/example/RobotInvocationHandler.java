package com.example;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class RobotInvocationHandler implements InvocationHandler {
	private BombDefuserRobot bombDefuserRobot;
	private int connectionWaveLenght;
	private int connectionAttempts = 3;

	public RobotInvocationHandler(BombDefuserRobot bombDefuserRobot, int connectionWaveLenght) {
		this.bombDefuserRobot = bombDefuserRobot;
		this.connectionWaveLenght = connectionWaveLenght;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		ensureConnectionWithRobot();
		return method.invoke(bombDefuserRobot, args);
	}

	private void ensureConnectionWithRobot() {
		if (bombDefuserRobot == null) {
			bombDefuserRobot = new BombDefuserRobot();
			bombDefuserRobot.connectWireless(connectionWaveLenght);
		}
		for (int i = 0; i < connectionAttempts; i++) {
			if (!bombDefuserRobot.isConnected()) {
				bombDefuserRobot.connectWireless(connectionWaveLenght);
			} else {
				break;
			}
		}
		if (!bombDefuserRobot.isConnected()) {
			throw new BadConnectionException(
					"No connection with remote bomb diffuser robot could be made after few attempts.");
		}
	}
}
