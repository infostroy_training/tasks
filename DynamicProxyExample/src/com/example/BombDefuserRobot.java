package com.example;

import java.util.Random;

public class BombDefuserRobot implements Robot{

	private int robotConfiguredWaveLength = 50;
	private boolean connected = false;

	public void moveForward(int steps) {
		System.out.println("Doing " + steps + " steps forward");
	}

	public void turnLeft() {
		System.out.println("Turning left");
	}

	public void turnRight() {
		System.out.println("Turning right");
	}

	public void defuseBomb() {
		System.out.println("Cutting red or blue wire...");
	}

	public void connectWireless(int connectionWaveLenght) {
		if (connectionWaveLenght < robotConfiguredWaveLength) {
			connected = immitateBadConnection();
		}
	}

	public boolean isConnected() {
		connected = immitateBadConnection();
		return connected;
	}

	private boolean immitateBadConnection() {
		return new Random().nextInt(10) < 8;
	}
}
