package com.example;

public class BadConnectionException extends RuntimeException {

	private static final long serialVersionUID = 7491700451199353328L;

	public BadConnectionException(String message) {
		super(message);
	}
}
