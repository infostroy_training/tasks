package controllers;

import java.util.List;

import javax.persistence.Query;

import models.Animal;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;
import views.html.animalsTemplate;

public class Application extends Controller {

    public Result index() {
        return ok(index.render("HELLO WORLD!"));
    }
    
    @Transactional
    public Result addAnimal(){
    	Animal animal = Form.form(Animal.class).bindFromRequest().get();
    	JPA.em().persist(animal);
    	return redirect(routes.Application.index());
    }
    
    @Transactional
    public Result getAnimals(){
    	Query query = JPA.em().createQuery("select e from Animal e");
    	List<Animal> animals = query.getResultList();
    	return ok(animalsTemplate.render(animals));
    }
}
