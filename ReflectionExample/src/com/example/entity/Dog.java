package com.example.entity;

import com.example.annotation.Column;
import com.example.annotation.Table;

@Table(name = "DOGS")
public class Dog {

	@Column(name = "dog_id")
	private Integer id;
	
	@Column(name = "name")
	private String name;

	private int age;

	public Dog() {
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
